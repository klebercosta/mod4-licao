package Licaomod4;

public class Carro {
	
	private String cor;
	private String modelo;
	private String marca;
	private int anoFabricacao;
	
	public Carro( String cor, String modelo, String marca, int anoFabricacao) {
		this.cor = cor;
		this.modelo = modelo;
		this.marca = marca;
		this.anoFabricacao = anoFabricacao;
	}
	
	public String getCor() {
		return cor;
	}
	public void setCor(String cor) {
		this.cor = cor;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public int getAnoFabricacao() {
		return anoFabricacao;
	}
	public void setAnoFabricacao(int anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
	}
	
	
	
	

}
