package Licaomod4;

import java.util.Scanner;

public class Opcionais {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("----Opcionais eleitos pelo Cliente----");
		System.out.println();
		System.out.print("Por favor escolha a cor desejada: ");
		String cor = scanner.nextLine();
		System.out.println();
		System.out.print("Por favor escolha o modelo desejado: ");
		String modelo = scanner.nextLine();
		System.out.println();
		System.out.print("Por favor escolha a marca desejada: ");
		String marca = scanner.nextLine();
		System.out.println();
		System.out.print("Por favor digite o ano de fabricação de sua escolha: ");
		int anoFabricacao = scanner.nextInt();
		System.out.println();
		
		Carro carronovo = new Carro(cor, modelo, marca, anoFabricacao);
				
				
		System.out.println("****Veja a escolha do Cliente****");
		System.out.println("----Cor: " + carronovo.getCor());
		System.out.println("----Modelo: " + carronovo.getModelo());
		System.out.println("----Marca: " + carronovo.getMarca());
		System.out.println("----Ano de fabricação: " + carronovo.getAnoFabricacao());
		
		System.out.println("***Concluído a escolha***!!!!!");



		
		
		
		
		
		
		
		scanner.close();
		

	}

}
